package main

import (
	"fmt"
	"log"
	"testing"
)

type locTest struct {
	ip       string
	name     bool
	response locationIDResponse
}

var locTests = []locTest{
	{"157.36.244.223", false, locationIDResponse{"1255744", "1270260", "1269750", "", "", ""}},
	{"223.238.211.65", false, locationIDResponse{"1272201", "1258899", "1269750", "", "", ""}},
	{"120.188.38.209", false, locationIDResponse{"1642911", "1642907", "1643084", "", "", ""}},
	{"157.35.239.66", false, locationIDResponse{"1260086", "1275715", "1269750", "", "", ""}},
	{"27.34.30.219", false, locationIDResponse{"1283240", "12095449", "1282988", "", "", ""}},
	{"49.32.197.91", false, locationIDResponse{"1275339", "1264418", "1269750", "", "", ""}},
}

var m int = len(locTests)

func TestLocationID(t *testing.T) {
	svc := setup()

	defer svc.db.Close()

	for _, tt := range locTests {
		resp, err := svc.LocationID(tt.ip, tt.name)

		if resp != tt.response {
			t.Errorf("Unexpected response for IP: %s", tt.ip)
		}

		if err != nil {
			t.Errorf("Error: %s", err)
		}
	}

}

func benchmarkLocationID(srv geolocate, b *testing.B) {
	for n := 0; n < b.N; n++ {
		tt := locTests[n%m]
		_, err := srv.LocationID(tt.ip, tt.name)

		if err != nil {
			log.Panic("Error:" + fmt.Sprint(err))
		}

	}
}

func BenchmarkLocationID100(b *testing.B) {
	svc := setup()

	defer svc.db.Close()
	benchmarkLocationID(svc, b)
}
func BenchmarkLocationID250(b *testing.B) {
	svc := setup()

	defer svc.db.Close()
	benchmarkLocationID(svc, b)
}
func BenchmarkLocationID500(b *testing.B) {
	svc := setup()

	defer svc.db.Close()
	benchmarkLocationID(svc, b)
}
