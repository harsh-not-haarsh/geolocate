package main

import (
	"errors"
	"log"
	"os"
	"reflect"

	"github.com/joho/godotenv"
	"github.com/oschwald/geoip2-golang"
)

func setup() geolocate {
	f := OpenFile()
	return geolocate{f}
}

// OpenFile Opens a file
func OpenFile() *geoip2.Reader {
	err := godotenv.Load("file.env")
	if err != nil {
		log.Fatalf("Error loading .env file")
	}
	file := os.Getenv("FILE")

	db, err := geoip2.Open(file)
	if err != nil {
		log.Fatal(err)
	}

	// f := Dbfile{}
	// f.db = db

	return db

}

// CheckError checks for errors in the response format
func CheckError(resp locationIDResponse, name bool) error {
	v := reflect.ValueOf(resp)
	count := 0
	for i := 0; i < v.NumField(); i++ {
		if v.Field(i).Interface() == "" {
			count++
		}
	}

	if count > 0 {
		if count == 6 {
			return ErrAbs
		} else if name && count < 6 {
			return ErrPar
		} else if !name && count > 3 {
			return ErrPar
		}
	}

	return nil
}

// ErrEmpty is returned when an input string is empty.
var ErrEmpty = errors.New("400")

// ErrPar is returned when an input string results in partial information
var ErrPar = errors.New("200")

// ErrAbs is returned when an input string results in no information
var ErrAbs = errors.New("204")
