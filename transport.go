package main

import (
	"context"
	"encoding/json"
	"net/http"

	"github.com/go-kit/kit/endpoint"
)

// Request format for LocationID
type locationIDRequest struct {
	IP   string
	Name bool
}

type locationIDResponse struct {
	CityID    string `json:"city_id,omitempty"`
	StateID   string `json:"state_id,omitempty"`
	CountryID string `json:"country_id,omitempty"`
	City      string `json:"city,omitempty"`
	State     string `json:"state,omitempty"`
	Country   string `json:"country,omitempty"`
}

// Endpoints are a primary abstraction in go-kit. An endpoint represents a single RPC (method in our service interface)
func makeLocationIDEndpoint(svc geolocate) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		req := request.(locationIDRequest)
		v, err := svc.LocationID(req.IP, req.Name)
		return v, err

	}
}

func decodeLocationIDRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request locationIDRequest
	ip := r.FormValue("ip")
	name := r.FormValue("name")

	request.IP = ip
	if name == "1" {
		request.Name = true
	} else {
		request.Name = false
	}
	return request, nil
}

func encodeResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	return json.NewEncoder(w).Encode(response)
}
