module go-env-ways

go 1.14

require (
	github.com/go-kit/kit v0.10.0
	github.com/joho/godotenv v1.3.0
	github.com/oschwald/geoip2-golang v1.4.0
)
