# Geolocate
Locating Location from IP using MaxMind



### Installation:
Requirements:

- Golang 1.14

- [GeoLite2-City.mmdb Dataset](https://dev.maxmind.com/geoip/geoip2/geolite2/)


### Procedure:


- Clone the Repository
```
   $ git clone https://bitbucket.org/harsh-not-haarsh/geolocate/src/master/
```


- Navigate to the Repository
```
   $ cd Geolocate
```


- Install Dependencies
```
   $ go get github.com/go-kit/kit/transport/http
```

```
   $ go get github.com/oschwald/geoip2-golang
```


### Running:

- Starting Server

```
   $ go run *.go
```


- Get Location Code
```
   $ curl  localhost:8000/id?ip=103.41.27.30
```


- Get Location Code Along with Name

```
   $ curl  localhost:8000/id?ip=103.41.27.30&name=1
```
