package main

import (
	"fmt"
	"log"
	"net"

	"github.com/oschwald/geoip2-golang"
)

// Geolocate provides Location information from IP.
type Geolocate interface {
	LocationID(string, bool) (locationIDResponse, error)
}

// geolocate is a concrete implementation of Geolocate
type geolocate struct {
	db *geoip2.Reader
}

func (svc *geolocate) LocationID(s string, name bool) (locationIDResponse, error) {
	if s == "" {
		log.Println(ErrEmpty)
		return locationIDResponse{}, ErrEmpty
	}

	ip := net.ParseIP(s)
	db := svc.db
	record, err := db.City(ip)
	if err != nil {
		log.Fatal(ErrEmpty)
	}

	resp := locationIDResponse{}
	resp.CityID = fmt.Sprint(record.City.GeoNameID)
	if len(record.Subdivisions) > 0 {
		resp.StateID = fmt.Sprint(record.Subdivisions[0].GeoNameID)
	}
	resp.CountryID = fmt.Sprint(record.Country.GeoNameID)

	if name {
		resp.City = record.City.Names["en"]
		resp.State = record.Subdivisions[0].Names["en"]
		resp.Country = record.Country.Names["en"]
	}

	return resp, CheckError(resp, name)
}
