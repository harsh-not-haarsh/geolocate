package main

import (
	"log"
	"net/http"

	httptransport "github.com/go-kit/kit/transport/http"
)

func main() {

	locationIDHandler := httptransport.NewServer(
		makeLocationIDEndpoint(setup()),
		decodeLocationIDRequest,
		encodeResponse,
	)

	http.Handle("/id", locationIDHandler)
	log.Fatal(http.ListenAndServe(":8000", nil))
}
